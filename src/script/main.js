const modal = new Modal();

const main = document.querySelector('.main');
const visitDesk = document.querySelector('.visit-desk');
const select = document.getElementById('doc-name');
const docForm = document.getElementById('doc-form');
const createBtn = document.getElementById('create-btn');
const docInput = document.getElementsByClassName('doc-input');
const createVisitBtn = document.getElementById('btn-create-visit');
const storageArr = JSON.parse(localStorage.getItem('visitCards'));
let showMore = false;

createVisitBtn.addEventListener('click', function () {

    for (let i = 0; i < docInput.length; i++) {
        docInput[i].value = '';
    }
    select.options[0].selected = 'selected';
    const inputCont = document.querySelector('.input-container');
    if (inputCont) {
        inputCont.remove();
    }
    if (createBtn.disabled === false) {
        createBtn.disabled = true;
        createBtn.style.backgroundColor = '#DEDEDE'
    }

    modal.open();
});


select.addEventListener('change', function () {

    const inputCont = document.createElement('div');
    inputCont.classList.add('input-container');
    docForm.insertBefore(inputCont, createBtn);
    switch (select.value) {
        case 'Dentist':
            DentistVisit.renderInputs(inputCont);
            break;

        case 'Therapist':
            document.querySelector('.input-container').remove();
            docForm.insertBefore(inputCont, createBtn);
            TherapistVisit.renderInputs(inputCont);
            break;

        case 'Cardiologist':
            document.querySelector('.input-container').remove();
            docForm.insertBefore(inputCont, createBtn);
            CardiologistVisit.renderInputs(inputCont);
            break;

        case 'choose-doc':
            document.querySelector('.input-container').remove();
            break;
    }
});

DoctorVisit.render();
main.onclick = DoctorVisit.showMore;
visitDesk.onclick = DoctorVisit.deleteCard;
window.onkeyup = DoctorVisit.checkInput;
window.onload = getFromLocalStorage;

//
// function({target}) {
//
//     emblem.onmousedown = function (e) {
//         /*
//         console.log(e);
//         console.log(this);
//         */
//         const coords = getCoords(this);
//
//         const shiftX = e.pageX - coords.left;
//         const shiftY = e.pageY - coords.top;
//
//         this.style.position = 'absolute';
//         document.body.appendChild(this);
//         moveAt(e);
//
//         this.style.zIndex = 1000; // над другими элементами
//
//         function moveAt(e) {
//             emblem.style.left = e.pageX - shiftX + 'px';
//             emblem.style.top = e.pageY - shiftY + 'px';
//         }
//
//         document.onmousemove = function (e) {
//             moveAt(e);
//         };
//
//         emblem.onmouseup = function () {
//             document.onmousemove = null;
//             emblem.onmouseup = null;
//         };
//
//     };
//
//     emblem.ondragstart = function () {
//         return false;
//     };
//
//     function getCoords(elem) {
//         const box = elem.getBoundingClientRect();
//         return {
//             top: box.top + pageYOffset,
//             left: box.left + pageXOffset
//         };
//     }
// }