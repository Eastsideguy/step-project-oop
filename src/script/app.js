class DoctorVisit {
    constructor(firstName, lastName, visitTarget, dateOfVisit, addComment) {
        this._firstName = firstName;
        this._lastName = lastName;
        this._visitTarget = visitTarget;
        this._dateOfVisit = dateOfVisit;
        this._addComment = addComment;
    }

    get firstName() {
        return this._firstName
    }

    get lastName() {
        return this._lastName
    }

    get visitTarget() {
        return this._visitTarget
    }

    get dateOfVisit() {
        return this._dateOfVisit
    }

    get addComment() {
        return this._addComment
    }

    static deleteCard({target}) {
        try {
            if (target.attributes.class && target.attributes.class.value === 'visit-card__cross-close') {
                const card = target.parentElement;
                addIndexProp(storageArr);
                setToLocalStorage(deleteFromLocalStorage(storageArr, 'index', whichChild(card)));

                if (card.previousElementSibling.id === 'empty-message' && card.nextElementSibling === null) {

                    document.getElementById('empty-message').style.display = 'block';
                    visitDesk.style.justifyContent = 'center';
                    visitDesk.style.alignItems = 'center';
                }

                card.remove();

            }
        } catch (e) {
            console.log(`${e.name}:${e.message}`);
        }
    }

    static showMore({target, path}) {
        if (target.attributes.class === undefined) {
            return true
        }
        try {
            if (target.attributes.class.value === 'visit-card__btn-showhide') {
                path[1].children[2].classList.toggle('active');
                showMore = !showMore;
                if (showMore) {
                    target.value = 'Hide x';
                } else {
                    target.value = 'Show more +';
                }
            }

        } catch (e) {
            console.log(`${e.name}:${e.message}`);
        }
    }

    static checkInput({target}) {

        if (target.attributes.class === undefined) {
            return true
        }
        try {
            if (target.attributes.class.value === 'doc-input') {
                const inputsArr = [];
                for (let i = 0; i < docInput.length; i++) {
                    if (docInput[i].value) {
                        inputsArr.push(docInput[i].value)
                    }
                }
                if (inputsArr.length === docInput.length) {
                    createBtn.disabled = false;
                    createBtn.style.backgroundColor = '#E1615D'
                } else {
                    createBtn.disabled = true;
                    createBtn.style.backgroundColor = '#DEDEDE';
                    createBtn.style.color = '#FFFFFF'
                }
            }
        } catch (e) {
            console.log(`${e.name}:${e.message}`);
        }
    }

    static render() {
        let visitCardsArr = [];
        const inputValueArr = [];

        if (storageArr) visitCardsArr = storageArr;

        createBtn.addEventListener('click', function () {
            inputValueArr.splice(0);
            const inputArr = document.querySelectorAll('.doc-input');
            inputArr.forEach(function (elem) {
                inputValueArr.push(elem.value)
            });
            const textareaValue = document.querySelector('.doc-textarea').value;
            inputValueArr.push(textareaValue);

            const visitCont = document.createElement('div');
            visitCont.classList.add('visit-card');
            visitDesk.appendChild(visitCont);
            deleteMessage();
            let doctor = 'doctor';
            switch (select.value) {
                case 'Therapist':
                    const visitTherapist = new TherapistVisit(...inputValueArr);
                    visitTherapist[doctor] = 'Therapist';
                    visitCardsArr.push(visitTherapist);
                    visitTherapist.render(visitCont);
                    break;

                case 'Dentist':
                    const visitDentist = new DentistVisit(...inputValueArr);
                    visitDentist[doctor] = 'Dentist';
                    visitCardsArr.push(visitDentist);
                    visitDentist.render(visitCont);
                    break;

                case 'Cardiologist':
                    const visitCardiologist = new CardiologistVisit(...inputValueArr);
                    visitCardiologist[doctor] = 'Cardiologist';
                    visitCardsArr.push(visitCardiologist);
                    visitCardiologist.render(visitCont);
            }
            setToLocalStorage(visitCardsArr);
        });

    }

}

class CardiologistVisit extends DoctorVisit {
    constructor(firstName, lastName, visitTarget, dateOfVisit, age, normalPressure, weightIndex, cardioDiseases, addComment) {
        super(firstName, lastName, visitTarget, dateOfVisit, addComment);
        this._normalPressure = normalPressure;
        this._weightIndex = weightIndex;
        this._cardioDiseases = cardioDiseases;
        this._age = age;
    }

    get normalPressure() {
        return this._normalPressure
    }

    get weightIndex() {
        return this._weightIndex
    }

    get cardioDiseases() {
        return this._cardioDiseases
    }

    get age() {
        return this._age
    }

    render(cont) {
        cont.innerHTML = `<div class="visit-card__patient-name">${this.firstName} ${this.lastName} </div>
                                            <div class="visit-card__doctor-name">${this.doctor}</div>
                                             <ul class="visit-card__hidden-property">
                                                <li><span class="visit-card__property">Target of visit:</span> ${this.visitTarget}</li>
                                                <li><span class="visit-card__property">Date of visit:</span> ${this.dateOfVisit}</li>
                                                <li><span class="visit-card__property">Age:</span> ${this.age}</li>
                                                <li><span class="visit-card__property">IBM:</span> ${this.weightIndex}</li>
                                                <li><span class="visit-card__property">Blood Pressure:</span> ${this.normalPressure}</li>
                                                <li><span class="visit-card__property">Previous diseases of the cardiovascular system:</span>  ${this.cardioDiseases}</li>
                                                <li><span class="visit-card__property">Additional comment:</span> ${this.addComment}</li>
                                            </ul>
                                            <div class="visit-card__line"></div>
                                            <input class="visit-card__btn-showhide" type="button" value="Show more +">
                               <span class="visit-card__cross-close">&times;</span>`;
    }

    static renderInputs(cont) {
        cont.innerHTML = `<input type="text"  class="doc-input" placeholder="First name">
                               <input type="text"  class="doc-input" placeholder="Last name">
                               <input type="text"  class="doc-input" placeholder="Target of your visit">
                               <input type="text"  class="doc-input" placeholder="Date of visit">
                               <input type="text"  class="doc-input" placeholder="Age">
                               <input type="text"  class="doc-input" placeholder="Normal blood pressure">
                               <input type="text"  class="doc-input" placeholder="Your BMI">
                               <input type="text"  class="doc-input" placeholder="Previous diseases of the cardiovascular system">
                               <textarea rows="4" maxlength="400" class="doc-textarea" placeholder="Enter your additional comments"></textarea>`;
    }
}

class DentistVisit extends DoctorVisit {
    constructor(firstName, lastName, visitTarget, dateOfVisit, dateOfLastVisit, addComment) {
        super(firstName, lastName, visitTarget, dateOfVisit, addComment);
        this._dateOfLastVisit = dateOfLastVisit;
    }

    get dateOfLastVisit() {
        return this._dateOfLastVisit
    }

    render(cont) {
        cont.innerHTML = `<div class="visit-card__patient-name">${this.firstName} ${this.lastName} </div>
                                            <div class="visit-card__doctor-name">${this.doctor}</div>
                                            <ul class="visit-card__hidden-property">
                                                <li><span class="visit-card__property">Target of visit:</span> ${this.visitTarget}</li>
                                                <li><span class="visit-card__property">Date of visit:</span> ${this.dateOfVisit}</li>
                                                <li><span class="visit-card__property">Date of last visit:</span> ${this.dateOfLastVisit}</li>
                                                <li><span class="visit-card__property">Additional comment:</span> ${this.addComment}</li>
                                            </ul>
                                            <div class="visit-card__line"></div>
                                            <input class="visit-card__btn-showhide" type="button" value="Show more +">
                                            <span class="visit-card__cross-close">&times;</span>`;
    }

    static renderInputs(cont) {
        document.querySelector('.input-container').remove();
        docForm.insertBefore(cont, createBtn);
        cont.innerHTML = `<input type="text" class="doc-input" placeholder="First name" >
                               <input type="text" class="doc-input" placeholder="Last name" >
                               <input type="text" class="doc-input" placeholder="Target of your visit" >
                               <input type="text" class="doc-input" placeholder="Date of visit" >
                               <input type="text" class="doc-input" placeholder="Date of your last visit" >
                               <textarea rows="4" maxlength="400"  class="doc-textarea" placeholder="Additional comments"></textarea>`;
    }
}

class TherapistVisit extends DoctorVisit {
    constructor(firstName, lastName, visitTarget, dateOfVisit, age, addComment) {
        super(firstName, lastName, visitTarget, dateOfVisit, addComment);
        this._age = age;
    }

    get age() {
        return this._age
    }

    render(cont) {
        cont.innerHTML = `<div class="visit-card__patient-name">${this.firstName} ${this.lastName} </div>
                                            <div class="visit-card__doctor-name">${this.doctor}</div>
                                            <ul class="visit-card__hidden-property">
                                                <li><span class="visit-card__property">Target of visit:</span> ${this.visitTarget}</li>
                                                <li><span class="visit-card__property">Age: </span> ${this.age}</li>
                                                <li><span class="visit-card__property">Date of visit:</span> ${this.dateOfVisit}</li>
                                                <li><span class="visit-card__property">Additional comment:</span>  ${this.addComment}</li>
                                            </ul>       
                                            <div class="visit-card__line"></div>
                                            <input class="visit-card__btn-showhide" type="button" value="Show more +">
                                            <span class="visit-card__cross-close">&times;</span>`;
    }

    static renderInputs(cont) {
        cont.innerHTML = `<input type="text"  class="doc-input" placeholder="First name" >
                               <input type="text"  class="doc-input" placeholder="Last name" >
                               <input type="text"  class="doc-input" placeholder="Target of your visit" >
                               <input type="text"  class="doc-input" placeholder="Date of visit" >
                               <input type="text"  class="doc-input" placeholder="Age" >
                               <textarea rows="4" maxlength="400"  class="doc-textarea" placeholder="Additional comments"></textarea>`;
    }
}

class Modal {
    constructor(nameElement = "modal") {
        this.nameElement = nameElement;
        this._closeEvents();
    }

    _closeEvents() {
        document.getElementById('modal-close').addEventListener("click", this.close);
        document.getElementById('modal-overlay').addEventListener("click", this.close);
        document.getElementById('create-btn').addEventListener('click', this.close);
    }

    open() {
        document.getElementById('modal-overlay').style.display = "block";
        document.getElementById('modal-modal').style.display = "block";
    }

    close() {
        document.getElementById('modal-overlay').style.display = "none";
        document.getElementById('modal-modal').style.display = "none";
    }
}

function deleteMessage() {
    const emptyMessage = document.getElementById('empty-message');
    if (document.querySelector('.visit-card')) {
        emptyMessage.style.display = "none";
        visitDesk.style.justifyContent = 'flex-start';
        visitDesk.style.alignItems = 'flex-start';
        visitDesk.style.flexWrap = 'wrap';
    }
    return true;
}

function setToLocalStorage(arr) {
    const serialObj = JSON.stringify(arr);
    localStorage.setItem(`visitCards`, serialObj);
}


function deleteFromLocalStorage(arr, attr, value) {
    let i = arr.length;
    while (i--) {
        if (arr[i]
            && arr[i].hasOwnProperty(attr)
            && (arguments.length > 2 && arr[i][attr] === value)) {

            arr.splice(i, 1);

        }
    }

    return arr;
}

function getFromLocalStorage() {
    if (storageArr) {

        storageArr.forEach(elem => {
            const visitCont = document.createElement('div');
            visitCont.classList.add('visit-card');
            visitDesk.appendChild(visitCont);

            deleteMessage();

            switch (elem.doctor) {
                case 'Therapist':
                    visitCont.innerHTML = `<div class="visit-card__patient-name">${elem._firstName} ${elem._lastName} </div>
                                            <div class="visit-card__doctor-name">${elem.doctor}</div>
                                            <ul class="visit-card__hidden-property">
                                                <li><span class="visit-card__property">Target of visit:</span> ${elem._visitTarget}</li>
                                                <li><span class="visit-card__property">Age: </span> ${elem._age}</li>
                                                <li><span class="visit-card__property">Date of visit:</span> ${elem._dateOfVisit}</li>
                                                <li><span class="visit-card__property">Additional comment:</span>  ${elem._addComment}</li>
                                            </ul>       
                                            <div class="visit-card__line"></div>
                                            <input class="visit-card__btn-showhide" type="button" value="Show more +">
                                            <span class="visit-card__cross-close">&times;</span>`;
                    break;
                case 'Dentist':
                    visitCont.innerHTML = `<div class="visit-card__patient-name">${elem._firstName} ${elem._lastName} </div>
                                            <div class="visit-card__doctor-name">${elem.doctor}</div>
                                            <ul class="visit-card__hidden-property">
                                                <li><span class="visit-card__property">Target of visit:</span> ${elem._visitTarget}</li>
                                                <li><span class="visit-card__property">Date of visit:</span> ${elem._dateOfVisit}</li>
                                                <li><span class="visit-card__property">Date of last visit:</span> ${elem._dateOfLastVisit}</li>
                                                <li><span class="visit-card__property">Additional comment:</span> ${elem._addComment}</li>
                                            </ul>
                                            <div class="visit-card__line"></div>
                                            <input class="visit-card__btn-showhide" type="button" value="Show more +">
                                            <span class="visit-card__cross-close">&times;</span>`;
                    break;
                case 'Cardiologist':
                    visitCont.innerHTML = `<div class="visit-card__patient-name">${elem._firstName} ${elem._lastName} </div>
                                            <div class="visit-card__doctor-name">${elem.doctor}</div>
                                             <ul class="visit-card__hidden-property">
                                                <li><span class="visit-card__property">Target of visit:</span>  ${elem._visitTarget}</li>
                                                <li><span class="visit-card__property">Date of visit:</span>  ${elem._dateOfVisit}</li>
                                                <li><span class="visit-card__property">Age:</span>  ${elem._age}</li>
                                                <li><span class="visit-card__property">IBM:</span>  ${elem._weightIndex}</li>
                                                <li><span class="visit-card__property">Blood Pressure:</span>  ${elem._normalPressure}</li>
                                                <li><span class="visit-card__property">Previous diseases of the cardiovascular system:</span>  ${elem._cardioDiseases}</li>
                                                <li><span class="visit-card__property">Additional comment:</span>  ${elem._addComment}</li>
                                            </ul>    
                                            <div class="visit-card__line"></div>  
                                            <input class="visit-card__btn-showhide" type="button" value="Show more +">
                               <span class="visit-card__cross-close">&times;</span>`;
            }
        });
    }
}

function whichChild(elem) {
    let i = 0;
    while ((elem = elem.previousElementSibling) != null) {
        ++i;
    }

    return i;
}

function addIndexProp(arr) {
    if (arr) {
        let countName = 1;
        for (let i = 0; i < arr.length; i++) {
            arr[i]['index'] = countName++;
        }
        return arr;
    }
}